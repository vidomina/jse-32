package com.ushakova.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AboutCommand extends AbstractCommand {

    @Override
    @Nullable
    public String arg() {
        return "-a";
    }

    @Override
    @Nullable
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("\n***About***");
        System.out.println("Name: ");
        System.out.println(Manifests.read("developer"));
        System.out.println("E-Mail: ");
        System.out.println(Manifests.read("email"));
    }

    @Override
    @NotNull
    public String name() {
        return "about";
    }

}
