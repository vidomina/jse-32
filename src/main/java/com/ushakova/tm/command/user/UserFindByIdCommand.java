package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserFindByIdCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Find user by id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter user id:");
        @NotNull final String userId = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println(user);
    }

    @Override
    @NotNull
    public String name() {
        return "user-find-by-id";
    }

}
