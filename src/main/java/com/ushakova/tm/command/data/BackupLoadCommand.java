package com.ushakova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.ushakova.tm.command.AbstractDataCommand;
import com.ushakova.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupLoadCommand extends AbstractDataCommand {

    @Override
    public @Nullable
    String arg() {
        return null;
    }

    @Override
    public @Nullable
    String description() {
        return "Load backup data.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final File file = new File(BACKUP_XML);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(BACKUP_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    public @NotNull
    String name() {
        return "bkp-load";
    }

}
